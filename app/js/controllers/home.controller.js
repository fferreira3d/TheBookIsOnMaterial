(function() {
    angular
        .module('app')
        .controller('HomeController', HomeController);

    function HomeController() {
        var vm = this;
        if (typeof localStorage === "undefined" || localStorage === null) {
            var LocalStorage = require('node-localstorage').LocalStorage;
            localStorage = new LocalStorage('./scratch');
        }
        vm.books = JSON.parse(localStorage.getItem('livros'));
        if(vm.books == null) {
            vm.books = [
                {'ISBN': '1234338437', 'Titulo': 'Livro Um', 'Ano': '2016', 'Autor': 'Eduardo Monteiro', 'Editora': 'Abril', 'Critica': 'Otimo Livro!'},
                {'ISBN': '1234338438', 'Titulo': 'Livro Numero Dois', 'Ano': '2013', 'Autor': 'Lobato Castro', 'Editora': 'Março', 'Critica': 'Recomendo...'},
                {'ISBN': '1234338439', 'Titulo': 'Livro Terceiro', 'Ano': '2009', 'Autor': 'Alves Peixoto', 'Editora': 'Agosto', 'Critica': 'Ruim!'},
            ];
            localStorage.setItem('livros', JSON.stringify(vm.books));
            console.log("INICIALIZANDO localStorage");
        }
        console.log("localStorage: " + vm.books);
    }
})();
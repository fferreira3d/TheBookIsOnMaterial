# Laboratório PUC - Oferta 5 - PPDS - Aula 05
###### Curso de Arquitetura de Software Distribuído - Oferta 5
###### Disciplina de Produtividade no Desenvolvimento de Software

----
# Respostas ao roteiro de atividades


## Tutorial 1 - Conceitos da Plataformas JavaScript
#### Questão 1 - Rode o [tutorial](http://yeoman.io/codelab/index.html). 
#### Questão 2 - Enumere o papel arquitetural de cada tecnologia explorada neste tutorial.
**Yeoman - Fountain WebApp**
 * Angular 2 (JavaScript Framework MV*, traz o processamento do controlador para o cliente tornando a aplicação mais leve pois eliminam boa parte das requisições entre cliente e servidor)
 * webpack with NPN ("MODULOS COM DEPENDNECIAS -> STATIC ASSETS")
 * TypeScript (Js Preprocessor, liguagem baseada em javascript)
 * CSS (CSS Preprocessor, facilita a organização visual das paginas, trazendo maior qualidade grafica e portabilidade)
 * Karma (Ferramenta de testes de unidade, traz automação de testes e melhora a manutenibilidade)

#### Questão 3 - Monte um diagrama UML de componentes que contenha as tecnologias estudadas nesse estudo dirigido
![Diagrama UML](DOCS/UML_TutorialUm.png)


## Tutorial 2 - SPA e Material Design
#### Questão 1 - Examine a [seguinte página](https://www.google.com/design/spec/material-design/introduction.html#) e explique o objetivo conceitual do Google.
Material design tem como objetivo unificar principios de um bom design, de forma a permitir o desenvolvimento de sistemas que se comporte em todas as plataformas e tamanhos de tela com a mesma experiencia. Ações e metodos de entrada são simples e intuitivos!

#### Questão 2 - Enumere as novidades de acessibilidade e usabilidade da biblioteca [MaterializeCSS](http://materializecss.com/about.html)
 * **Onboarding, feature discovery, and gesture education**: ajuda os usuários a começar a usar o interface do usuário de um aplicativo e descrobir novas funcionalidades e recursos da aplicação em momentos contextualmente relevantes.
 * **Paleta de cores**: Uso de cores que representam ação (verde = Aceitar; vermelho = Cancelar), alem de opções mais especificas do que o simples "Sim" e "Não".
 * **Movimento**: movimento indica ação


## Projeto Desafio
O projeto desenvolvido conforme a solicitação do roteiro, apresenta uma Simple Page Aplication baseado nos conceitos de usaabilidade e acessibilidade do Material Design.
Foi utilizado o [generator-angular-materializecss](https://github.com/cmtp/generator-angular-materializecss) do Yeoman, para uma rapida configuração inicial do projeto. O editor de codigo Visual Studio Code ajudou no meu primeiro contato com o desenvolvimento de aplicações Java Script. A persistência dos dados é feita pelo 'localstorage' do navegador.

##### Install

`npm install node-localstorage`

`npm install && bower install`

##### Run

`gulp`